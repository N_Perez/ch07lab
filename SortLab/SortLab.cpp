/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 7 Lab 1 - Sort Lab
*/



#include <iostream>
#include "ldeque.h"

using namespace std;

void fourSorts(string filename, LDeck<int> deck);

int main()
{
	LDeck<int> deck;

	fourSorts("numbersinOrder.txt", deck);
	fourSorts("numbersReverseOrder.txt", deck);
	fourSorts("randomNumbers01.txt", deck);
	fourSorts("randomNumbers02.txt", deck);

	system("pause");
	return 0;
}


void fourSorts(string filename, LDeck<int> deck)
{
	//Takes a filename and a deck, uses the deck to load the file, sort it, print the sorted deck out, then load the same file again, sort it using another sort, print the sorted deck out, and so on for four different sorts.

	deck.fileIn(filename);
	deck.sortBubble();
	deck.readOut();

	deck.fileIn(filename);
	deck.sortInsertion();
	deck.readOut();

	deck.fileIn(filename);
	deck.sortSelection();
	deck.readOut();

	deck.fileIn(filename);
	deck.sortMerge();
	deck.readOut();
}